# -*- encoding: utf-8 -*-
"""
@File    : LoadData.py
@Time    : 2020/0/13 22:50
@Author  : zx
@Email   : zhaoxiao@tju.edu.cn
@Software: PyCharm 
"""
# from tensorflow.python.keras.utils import get_file
import gzip
import numpy as np
import matplotlib.pyplot as plt

def load_data():
    # base = "file:///C:/Users/zx/Desktop/项目/ClothingRetrieval/data/"
    # base = "file:///D:/fashion-mnist/data/fashion/"
    base = "./fashion/"
    files = [
        'train-labels-idx1-ubyte.gz', 'train-images-idx3-ubyte.gz',
        't10k-labels-idx1-ubyte.gz', 't10k-images-idx3-ubyte.gz'
    ]

    # paths = []
    # for fname in files:
    #     paths.append(get_file(fname, origin=base + fname))

    with gzip.open(base+files[0], 'rb') as lbpath:
        y_train = np.frombuffer(lbpath.read(), np.uint8, offset=8)

    with gzip.open(base+files[1], 'rb') as imgpath:
        x_train = np.frombuffer(
            imgpath.read(), np.uint8, offset=16).reshape(len(y_train), 28, 28)

    with gzip.open(base+files[2], 'rb') as lbpath:
        y_test = np.frombuffer(lbpath.read(), np.uint8, offset=8)

    with gzip.open(base+files[3], 'rb') as imgpath:
        x_test = np.frombuffer(
            imgpath.read(), np.uint8, offset=16).reshape(len(y_test), 28, 28)

    return (x_train, y_train), (x_test, y_test)
if __name__ == '__main__':
    (x_train, y_train), (x_test, y_test) = load_data()
    print(x_train.shape)
    print(y_train.shape)
