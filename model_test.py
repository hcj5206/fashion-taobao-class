"""
标签	所代表的意思
0	短袖圆领T恤
1	裤子
2	套衫
0	连衣裙
4	外套
5	凉鞋
6	衬衫
7	1
8	包
9	短靴
"""
# please note, all tutorial code are running under python3.5.
# If you use the version like python2.7, please modify the code accordingly

# 6 - CNN example

# to try tensorflow, un-comment following two lines
# import os
# os.environ['KERAS_BACKEND']='tensorflow'
import keras
import numpy as np
np.random.seed(1337)  # for reproducibility
from keras.datasets import mnist
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Dense, Activation, Convolution2D, MaxPooling2D, Flatten
from keras.optimizers import Adam
from keras.models import load_model
import LoadData
# download the mnist to the path '~/.keras/datasets/' if it is the first time to be called
# training x shape (60000, 28x28), Y shape (60000, ). test x shape (10000, 28x28), Y shape (10000, )
(x_train, y_train), (x_test, y_test) = LoadData.load_data()

# data pre-processing
x_train = x_train.reshape(-1, 1,28, 28)/255.
x_test = x_test.reshape(-1, 1,28, 28)/255.
y_train = np_utils.to_categorical(y_train, num_classes=10)
y_test = np_utils.to_categorical(y_test, num_classes=10)

# Another way to build your CNN
# cloth_dict={
# 0	:'短袖圆领T恤',
# 1	:'裤子',
# 2	:'套衫',
# 3	:'连衣裙',
# 4	:'外套',
# 5	:'凉鞋',
# 6	:'衬衫',
# 7	:'1',
# 8	:'包',
# 9	:'短靴'
# }
cloth_dict={
0	:'牛仔裤',
1	:'鞋子',
2	:'双肩背包',
3   :'长袖外套',
4   :'鸭舌帽',
}

model = load_model('my_model.h5')
# 训练模型
# 评估模型
import cv2
import matplotlib.pyplot as plt

'''
<class 'numpy.ndarray'>
(1, 28, 28)
plt.figure()
plt.imshow(X_train[0].reshape(28,28))
plt.colorbar()
plt.grid(False)
plt.show()
'''
# for i in range(20,60):
#     plt.figure()
#     print("预测值  ：", cloth_dict[int(np.argmax(model.predict(x_test[i:i+1]), axis=1))])  # 打印最大概率对应的标签
#     print("预测值  ：", cloth_dict[int(np.argmax(y_test[i]))])  # 打印最大概率对应的标签
#     plt.imshow(x_test[i].reshape(28, 28))
#     plt.colorbar()
#     plt.grid(False)
#     plt.show()
#     a=input("1")
for i in range(0,30):
    img=cv2.imread("picture/3/3-%s.jpg"%i)
    gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    gray=cv2.resize(gray,(220,220))
    gray = gray.reshape(-1, 1,220, 220)/255.
    print("预测值  ：", cloth_dict[int(np.argmax(model.predict(gray), axis=1))])
    print("真实  ：",0)		# 打印最大概率对应的标签
    cv2.imshow("1",img)
    cv2.waitKey(0)
    # cv2.imshow("1",img)
    # cv2.waitKey(0)
